
function function_invert(input_object) {                             // declaring the invert function     
    let result_object = {}                                           // to store the result

    if (typeof input_object === 'object') {                         // to check the input type

        for (let element in input_object) {                          // going through each key
            result_object[input_object[element]] = element             // passing the data into empty object
        } return result_object                                      // returning result
        } else {
            return 'Invalid input'
        }
} 

module.exports = function_invert                               // exporting the function