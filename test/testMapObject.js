
const mapObjects = require('../mapObject.js')       // importing the function

const input_object = {                            // data 
    'age_1': 25,
    'age_2': 32,
    'age_3': 45,
    'age_4': 61
}
const new_object = mapObjects(input_object,cb_function = item => {             // input an object, test case 1
    return item + 10
})

console.log(new_object)

const answer = mapObjects('hi',cb_function = item => {                        // test case 2
    return item*5
})
console.log(answer)

const answer_1 = mapObjects(true,cb_function = item => item+'hi')             // test case 3
console.log(answer_1)