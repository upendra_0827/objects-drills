
const pairs = require('../pairs')                            // importing the function

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }        // data

console.log(pairs(testObject))                       // input an object, test acse 1
console.log(pairs(true))                       // test case 2
console.log(pairs(0))                                 // test case 3
console.log(pairs('hi'))                          // test case 4