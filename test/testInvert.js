
const invert = require('../invert.js')                                // importing the function

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }    // data

console.log(invert(testObject))                                   // input an object, test case 1
console.log(invert('hi'))                                         // test case 2
console.log(invert(0))                                          // test case 3
console.log(invert(true))                                        // test case 4
