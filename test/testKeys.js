
const key_array = require('../keys.js')                            // importing the function

const information = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

console.log(key_array(information))                           // input an object, test case 1
console.log(key_array('hi'))                                  // test case 2
console.log(key_array(true))                                    // test case 3
console.log(key_array(0))                                      // test case 4