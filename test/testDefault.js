const function_default = require("../default.js")               // importing the function

var iceCream = {flavor: "chocolate"};                       // data 1
const data_object = {flavor: "vanilla", sprinkles: "lots"}    // data 2


console.log(function_default(iceCream,data_object))           // input an object, test case 1
console.log(function_default(0,data_object))                 // test case 2
console.log(function_default(iceCream,'hi'))                 // test case 3
console.log(function_default(true,0))                         // test case 4