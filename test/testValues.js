const values = require('../values')                      // importing the function

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };      // data

console.log(values(testObject))                        // input an object, test case 1
console.log(values('hi'))                             // test case 2
console.log(values(0))                                 // test case 3
console.log(values(true))                            // test case 4