
function function_default(obj,default_props) {                       // declaring the function

    if (typeof obj === 'object' && typeof default_props === 'object') {     // checking the type of input
    
        for (let key in obj) {                              // going through each key
            default_props[key] = obj[key]
        } return default_props                                // returning the result
    } else {
        return 'Invalid input'
    }
}

module.exports = function_default                            // exporting the function