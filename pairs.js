
function pairs(obj) {                                           // declaring function pairs
    if (typeof obj === 'object') {                           // checking the type of input
    let new_array = []                                        // to store the result

    for (let element in obj) {                                  // going through each key
        new_array.push([element,obj[element]])                 // pushing into an empty array
    } return new_array                                         // returning the resultant array
    } else {
        return 'Invalid input'
    }
}

module.exports = pairs                                          // exporting the function