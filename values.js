
function values(obj) {                                             // declaring a function
    if (typeof obj === 'object') {                                 // checking the type of input
        let values_array = []                                      // to store the output
        for (let key in obj) {                                    // going through each key
            values_array.push(obj[key])                           // pushing the result into anj array
        } return values_array                                       // returning the result
    } else {
        return 'Invalid input'
    }
}

module.exports = values                                         // exporting the function