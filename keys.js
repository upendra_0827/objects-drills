
function keys(input_object) {                                   // declaring the function

    if (typeof input_object === 'object') {                     // checking the type of input
    let new_array = []                                         // to store the output

    for (let element in input_object) {                        // going through each key   
        new_array.push(element.toString())                      // pushing the output into array
    } return new_array                                          // returning the result   
    } else { 
        return 'Invalid input'
    }
}

module.exports = keys                                              // exporting the function