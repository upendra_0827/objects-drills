
function map_object(obj,cb) {                            // declaring a function

    if (typeof obj === 'object') {                      // checking the type of input
        for (let key in obj) {                          // going through each key
            obj[key] = cb(obj[key])                  
        } return obj                                    
    } else {
        return 'Invalid input'
    }
} 

module.exports = map_object                                  // exporting the function